FROM python:3.7.7-alpine3.12

WORKDIR /app

COPY src/ ./

RUN pip install -r requirements.txt && pip install -e .

CMD ["just_run_it"]