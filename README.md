# A project for testing gitlab's capabilities.

## What kind of features does this project currently have

### It builds images both for deployment and testing.

As you can see in the `.gitlab-ci.yml` file in the build stage we build 2 images. One for testing and another one for deployments.

### Runs tests

It runs tests defined both in the tests folder and also doctests making sure that the documentation
is correct. 

### Deployment

We have a run stage where it basically runs a service somewhere where a `gitlab-runner` is present.

Also we have a deploy stage which deploys the documentation [here](https://jakab922.gitlab.io/gitlab_ci_tester/).
