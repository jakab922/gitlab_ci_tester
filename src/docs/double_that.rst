Documentation for the great double_that function
================================================

.. currentmodule:: the_package.stuff

How to work with double that
----------------------------

.. autofunction:: double_that