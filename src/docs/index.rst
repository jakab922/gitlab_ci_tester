.. JustATest documentation master file, created by
   sphinx-quickstart on Fri Jul  3 18:43:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JustATest's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   
   double_that


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
