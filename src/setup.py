from setuptools import setup, find_packages


def load_requirements() -> str:
    with open("requirements.txt", "r") as f:
        return [line for line in f.readlines() if len(line) > 0]


setup(
    name="Just for testing",
    version="0.0.1",
    url="https://gitlab.com/jakab922/gitlab_ci_tester",
    author="Daniel Papp",
    author_email="author@gmail.com",
    description="Just for testing purposes",
    install_requires=load_requirements(),
    packages=["the_package"],
    entry_points={
        "console_scripts": [
            "just_run_it=the_package.stuff:main"
        ]
    }
)

