from the_package.stuff import double_that
from random import randint
import pytest

count = 20

@pytest.mark.parametrize("x", [randint(1, 1000) for _ in range(count)])
def test_double_that(x: int):
    assert double_that(x)
