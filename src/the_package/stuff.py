from flask import Flask
from random import randint


app = Flask(__name__)


def double_that(num: int) -> int:
    """Believe it or not but this amazing function can double any number

    :param num: It's the number that will get doubled

    :returns: Two times *num*!!!

    :Example:

    >>> from the_package.stuff import double_that
    >>> num = 12
    >>> double_that(num)
    24
    """

    return 2 * num


@app.route("/")
def hello_world():
    visitor_count = randint(1, 100)
    return f"Hello, your the {double_that(visitor_count)}th on this site!"


def main():
    app.run(debug=True, host="0.0.0.0")
    
